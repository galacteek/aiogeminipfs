import os.path
import aioipfs
import traceback
import mimetypes

from .. import Status, GEMINI_MEDIA_TYPE
from . import _RequestHandler, Request, Response


async def cat(client: aioipfs.AsyncIPFS,
              path: str):
    try:
        guess = mimetypes.guess_type(path)
        mtype = guess[0] if guess[0] else GEMINI_MEDIA_TYPE
        return await client.cat(path), mtype
    except (aioipfs.APIError, aioipfs.UnknownAPIError) as e:
        raise e


async def data_response(req, data, content_type=GEMINI_MEDIA_TYPE):
    response = Response()
    response.content_type = content_type
    response.start(req)
    await response.write(data)
    await response.write_eof()
    return response


def create_unixfs_server(
    client: aioipfs.AsyncIPFS,
    ipfs_root_path: str,
    buffersize: int = 2 ** 20
) -> _RequestHandler:
    """
    Very basic IPFS UnixFs handler for aiogemini.

    TODO: Implement buffering
    """
    async def handle_request(req: Request) -> Response:
        ipfs_base_path = ipfs_root_path.rstrip('/')
        path = req.url.path.lstrip('/')
        target = os.path.join(ipfs_base_path, path)

        try:
            data, ctype = await cat(client, target)
            assert data is not None

            return await data_response(req, data, ctype)
        except aioipfs.APIError:
            listing = []
            try:
                # Try with .gmi suffix
                gmi = os.path.join(ipfs_base_path, f'{path}.gmi')
                data, ctype = await cat(client, gmi)
                assert data is not None

                return await data_response(req, data, ctype)
            except Exception:
                pass

            try:
                resp = await client.ls(target)
                assert 'Objects' in resp
                links = resp['Objects'][0]['Links']
                names = [e['Name'] for e in links]

                if 'index.gmi' in names:
                    # Render the index

                    index = os.path.join(ipfs_base_path, 'index.gmi')
                    data, ctype = await cat(client, index)

                    if data:
                        return await data_response(req, data, ctype)
                    else:
                        raise Exception('Index is invalid')

                for entry in links:
                    if entry['Type'] == 1:
                        listing.append(f"=> {entry['Name']}/")
                    else:
                        listing.append(f"=> {entry['Name']}")
            except Exception:
                traceback.print_exc()
                return Response(Status.NOT_FOUND, "No such file")
            else:
                if listing:
                    return Response(data='\n'.join(listing).encode('utf-8'))
                else:
                    return Response(data='Empty directory'.encode('utf-8'))

        except FileNotFoundError:
            return Response(Status.NOT_FOUND, "No such file")

    return handle_request
